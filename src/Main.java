import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] sirDeNumere = {1, 4, 6, 7, 8, 10, 13, 17, 19, 20, 22, 23, 30, 45};

        Operation obj = new Operation();
        obj.afisareSir(sirDeNumere);

        System.out.println("Intoduceti numarul ce urmeaza a fi verificat: ");
        Scanner sc = new Scanner(System.in);
        int suma = sc.nextInt();
        obj.testareArray(suma, sirDeNumere);

    }
}
