import java.util.Scanner;

public class Operation {

    public void afisareSir(int[] sirDeNumere) {
        System.out.println("Sirul nostru este format din: ");
        for (int x = 0; x < sirDeNumere.length; x++) {
            System.out.print(" " + sirDeNumere[x]);
        }
        System.out.println(" ");
        System.out.println("==============================================");
    }


    public boolean testareArray(int suma, int[] sirDeNumere) {

        boolean test = false;
        int sumaTest;

        int x = 0;
        int lungimeSir = sirDeNumere.length - 1;

        while (x < lungimeSir) {
            sumaTest = sirDeNumere[x] + sirDeNumere[lungimeSir];
            if (sumaTest == suma) {
                test = true;
                break;
            }
            if (sumaTest < suma) {
                x++;
            }
            if (sumaTest > suma) {
                lungimeSir--;
            }
        }
        System.out.println("Rezultatul verificarii este: " + test);
        return test;
    }
}
